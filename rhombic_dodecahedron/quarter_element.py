from rhombic_dodecahedron.graph import Graph
from rhombic_dodecahedron.molecule import Molecule

class Quarter_element(Graph):
    def __init__(self, location, orientation, axis, subcubes=(0,1)):
        size = [1, 1, 1]
        size[axis] = 2 * size[axis]
        super().__init__(size)

        self.location = location

        #orientation works as follows:
        #   0 -> RD center closest to parallel-axis (bottom (bottom for z-axis is closer to x-axis))
        #   1 -> RD center furthest from parallel-axis (bottom)
        #   2 -> RD center closest to parallel-axis (top)
        #   3 -> RD center furthest from parallel-axis (top)
        self.orientation = orientation
        self.axis = axis

        #specify which subcubes the quarter element makes up
        self.subcubes = subcubes
        
        super().create_graph()
        self.create_graph()

    def create_graph(self):
        #tuple to get the molecule orientations right for each quarter element orientation and axis
        #first index: axis
        #second index: orientation
        molecule_info = \
                        (((0,0), (1,1), (2,2), (3,3)),
                         ((1,0), (1,0), (3,2), (3,2)),
                         ((2,0), (2,0), (3,1), (3,1)))

        v = []

        for i in range(2):
            molecule_orientation = molecule_info[self.axis][self.orientation][i]
            v.append(2*molecule_orientation)

            molecule = Molecule(0, molecule_orientation, self.subcubes[i])
            if super().check_availabilities(molecule, i*(self.axis + 1)) == False:
                self.node_list = []
                self.edge_list = []
                super().create_graph()
                return
            
            for edge in molecule.edge_list:
                self.edge_list.append(edge)

            for j, node in enumerate(molecule.node_list[0]):
                if node != True:
                    self.node_list[i][j] = self
        
        self.edge_list.append(((self.subcubes[0],v[0]),(self.subcubes[1],v[1])))

    def apply_to_global(self, graph):
        graph.apply_subgraph_to_graph(self)
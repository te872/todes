import plotly.graph_objects as go
import numpy as np
from math import floor
from copy import deepcopy
from scipy import sparse
from scipy.sparse import linalg
from rhombic_dodecahedron.graph import Graph
from rhombic_dodecahedron.lattice import Lattice
from rhombic_dodecahedron.interface import Interface

import matplotlib.pyplot as plt

class Simulation():

    def __init__(self, model_type, p, f):
        self.p = p

        #wall
        # self.structure_size = (10,2,10)
        # self.voids = [[0], [(0,0,0)]]
        # self.supported = [[(0.0, 0.6), (-np.inf, np.inf), (0.0, 0.6)], [(9.1, 9.6), (-np.inf, np.inf), (0.0, 0.6)]]
        # self.applied_forces = [[(-np.inf, np.inf), (-np.inf, np.inf), (9.0, 9.6)]]

        #long corridor
        # self.structure_size = (8,15,7)
        # self.voids = [[0], [(0,0,0)]]
        # self.supported = [[(0.0, 1.6), (-np.inf, np.inf), (0.0, 0.6)], [(6.1, 7.6), (-np.inf, np.inf), (0.0, 0.6)]]
        # self.applied_forces = [[(-np.inf, np.inf), (-np.inf, np.inf), (6.0, 6.6)]]

        #tall corridor
        # self.structure_size = (8,5,12)
        # self.voids = [[2], [(4,5,4)]]
        # self.supported = [[(0.0, 1.6), (-np.inf, np.inf), (0.0, 0.6)], [(6.1, 7.6), (-np.inf, np.inf), (0.0, 0.6)]]
        # self.applied_forces = [[(-np.inf, np.inf), (-np.inf, np.inf), (11.0, 11.6)]]

        #room 1 door
        # self.structure_size = (8,8,10)
        # self.voids = [[3, 18], [(2,2,2), (4,4,3)]]
        # self.supported = [[(-np.inf, np.inf), (-np.inf, np.inf), (0.0, 0.6)]]
        # self.applied_forces = [[(-np.inf, np.inf), (-np.inf, np.inf), (9.0, 9.6)]]

        #room 4 doors
        # self.structure_size = (8,8,9)
        # self.voids = [[3, 18, 24], [(2,8,2), (4,4,3), (8,2,2)]]
        # self.supported = [[(-np.inf, np.inf), (-np.inf, np.inf), (0.0, 0.6)]]
        # self.applied_forces = [[(-np.inf, np.inf), (-np.inf, np.inf), (8.0, 8.6)]]

        self.structure_size = (5,2,5)
        self.voids = [[0], [(0,0,0)]]
        self.supported = [[(0.1, 0.6), (0.0, 1.6),  (0.0, 0.6)],[(4.1, 4.6), (0.0, 1.6),  (0.0, 0.6)]]
        self.applied_forces = [[(0, 0), (0, 0), (0, 0)]]

        # self.structure_size = (20,5,5)
        # self.voids = [[0], [(0,0,0)]]
        # self.supported = [[(0.1, 0.6), (-np.inf, np.inf),  (0.0, 0.6)],[(4.1, 4.6), (-np.inf, np.inf),  (0.0, 0.6)],[(9.1, 10.6), (-np.inf, np.inf),  (0.0, 0.6)],[(15.1, 15.6), (-np.inf, np.inf),  (0.0, 0.6)],[(19.1, 19.6), (-np.inf, np.inf),  (0.0, 0.6)]]
        # self.applied_forces = [[(0.0, 0.0), (0.0, 0.0), (0.0, 0.0)]]


        self.spacing = 1.0
        self.iteration_count = 200
        self.convergence = 1e-2
        self.model_type = model_type

        self.lattice = Lattice(self.structure_size, self.supported, self.applied_forces, self.spacing, f, self.voids[0], self.voids[1])
        self.graph = self.lattice.graph
        self.element_list = self.lattice.element_list

        self.interface_list = []
        self.u = []
        self.f = []
        self.directory_path = "rhombic_dodecahedron/post-processing/"

        f = open(self.directory_path + "displacements.dat", "w")
        f.write("")
        f.close()
        f = open(self.directory_path + "elements.dat", "w")
        f.write("")
        f.close()
        f = open(self.directory_path + "convergence.csv", "w")
        f.write("")
        f.close()

        self.interface_list = self.set_interfaces()

        for i, element in enumerate(self.element_list):
            element.id = i

            f = open(self.directory_path + "elements.dat", "a")
            f.write(str(i) + " " + str(element.center_of_mass[0]) + " " + str(element.center_of_mass[1]) + " " + str(element.center_of_mass[2]) + "\n")
            f.close()

        self.u.append(np.array([0.0] * 6 * len(self.element_list)))
        self.f.append(np.array([0.0] * 6 * len(self.element_list)))

        # self.show_3d_graph(0)

    def set_interfaces(self):
        interface_list = []

        for i in range(self.graph.graph_size[0] * self.graph.graph_size[1] * self.graph.graph_size[2]):
            for j in range(1,8,2):
                if type(self.graph.node_list[i][j]).__bases__[0] == Graph:
                    for k in range(1,8,2):
                        if j != k:
                            if type(self.graph.node_list[i][k]).__bases__[0] == Graph:
                                element0 = self.graph.node_list[i][j]
                                element1 = self.graph.node_list[i][k]
                                local_index0 = element0.get_local_index(i, self.graph)
                                local_index1 = element1.get_local_index(i, self.graph)
                                interface_length = np.linalg.norm(element0.node_coords[local_index0][j] - element1.node_coords[local_index1][k])
                                interface_list.append(Interface(i, [j,k], self.spacing, self.graph, interface_length))
        
        return interface_list

    def calculate_kinematics(self, u, v, a, dt):
        u0 = deepcopy(u)
        v0 = deepcopy(v)
        
        f = open(self.directory_path + "displacements.dat", "a")

        for element in self.element_list:

            for j in range(6):
                if element.constraints[j]:
                    u[6*element.id + j] = 0.0
                    v[6*element.id + j] = 0.0
                else:
                    u[6*element.id + j] = 0.5*a[6*element.id + j]*dt**2 + v0[6*element.id + j]*dt + u0[6*element.id + j]
                    v[6*element.id + j] = a[6*element.id + j]*dt + v0[6*element.id + j]

                
                f.write("{:.10f}".format(u[6*element.id + j]) + " ")

            f.write(str(element.weight) +"\n")

            element.rotate_element(u[6*element.id+3:6*element.id+6] - u0[6*element.id+3:6*element.id+6])
            element.translate_element(u[6*element.id:6*element.id+3] - u0[6*element.id:6*element.id+3])

        f.close()

        for interface in self.interface_list:
            interface.node_coords = interface.update_node_coords(self.graph)
            interface.l = np.linalg.norm(interface.node_coords[0] - interface.node_coords[1])
            if interface.l > 1.00 * interface.l0 or interface.l < 0.1 * interface.l0:
                interface.active = False
            else:
                interface.active = True

        return u, v
    
    def assemble_system(self, E, nu):
        E_0 = 0.0
        E_1 = 0.0
        C = 0.0
        b = 100.0
        v_limit = [False] * len(self.element_list)
        t = 0.0

        u = np.zeros(6 * len(self.element_list))
        v = np.zeros(6 * len(self.element_list))
        f = np.zeros(6 * len(self.element_list))
        df = np.ones(6 * len(self.element_list))
        df_1 = np.zeros(6 * len(self.element_list))

        for element in self.element_list:
            for i, subcube in enumerate(element.node_coords):
                for node in subcube.keys():
                    element.node_coords[i][node] = element.calculate_global_coordinates(i, node, self.graph)
            f[6 * element.id:6 * element.id + 6] = element.applied_forces
            if self.model_type == 0:
                f[6 * element.id + 2] -= element.mass * element.weight ** self.p * 9.81
            else:
                f[6 * element.id + 2] -= element.mass * 9.81

            element.stiffness_matrices = {}
            element.other_ids = []
            element.other_weights = []
        
        for interface in self.interface_list:
            interface.node_coords = interface.update_node_coords(self.graph)
            interface.l = np.linalg.norm(interface.node_coords[0] - interface.node_coords[1])
            interface.active = True

        self.f = f

        #use time-independent analysis
        if self.model_type == 0:
            K = self.assemble_K(E, nu, save=True)
            K = K[K.getnnz(1)>0][:,K.getnnz(0)>0]

            f_free = np.array([0.0] * K.shape[0])
            i = 0
            for element in self.element_list:
                for j in range(6):
                    if element.constraints[j]:
                        continue
                    
                    f_free[i] = f[6*element.id + j]
                    i += 1

            u_free = sparse.linalg.spsolve(K, f_free)

            i = 0
            for element in self.element_list:
                for j in range(6):
                    if element.constraints[j]:
                        continue
                    
                    u[6*element.id + j] = u_free[i] 
                    i += 1

                element.rotate_element(u[6*element.id+3:6*element.id+6])
                element.translate_element(u[6*element.id:6*element.id+3])

            self.u = u

            for interface in self.interface_list:
                interface.node_coords = interface.update_node_coords(self.graph)
                interface.l = np.linalg.norm(interface.node_coords[0] - interface.node_coords[1])

            file = open(self.directory_path + "displacements.dat", "a")

            for i in range(len(self.element_list)):

                for j in range(6):
                    file.write("{:.10f}".format(u[6*i + j]) + " ")
                file.write(str(self.element_list[i].weight) +"\n")

            file.close()

        #use time-dependent analysis
        else:
            M, K, dt = self.construct_matrices(E, nu,)
            loop = 0
            while(np.linalg.norm(df) >= self.convergence * np.linalg.norm(df_1) and loop < self.iteration_count):

                #use adaptive global damping
                if self.model_type == 1:
                    a = sparse.linalg.inv(M).dot(f - b*C*v - K @ u)
                    u, v = self.calculate_kinematics(u, v, a, dt)

                    E_0 = deepcopy(E_1)
                    E_1 = np.matmul(np.transpose(v), (M @ v))
                    C = (E_1 - E_0)/(dt * 6*len(self.element_list))

                #use local damping
                elif self.model_type == 2:
                    # file = open(self.directory_path + "displacements.dat", "a")
                    u0 = deepcopy(u)
                    for element in self.element_list:
                        df_element = f[6*element.id : 6*element.id + 6] - K[6*element.id : 6*element.id + 6, :] @ u
                        for i in range(6):
                            if element.constraints[i] == False:
                                if v[6*element.id+i] < 0.0:
                                    sgn_vi = -1.0
                                elif v[6*element.id+i] > 0.0:
                                    sgn_vi = 1.0
                                else:
                                    sgn_vi = 0.0

                                fd = 0.8*abs(df_element[i])*sgn_vi
                                v[6*element.id+i] = v[6*element.id+i] + (df_element[i] - fd) * (dt/element.mass_matrix[i,i])
                                u[6*element.id+i] = u[6*element.id+i] + v[6*element.id+i] * dt

                        #     file.write("{:.10f}".format(u[6*element.id + i]) + " ")
                        # file.write(str(element.weight) +"\n")

                        element.rotate_element(u[6*element.id+3:6*element.id+6] - u0[6*element.id+3:6*element.id+6])
                        element.translate_element(u[6*element.id:6*element.id+3] - u0[6*element.id:6*element.id+3])

                    # file.close()

                    for interface in self.interface_list:
                        interface.node_coords = interface.update_node_coords(self.graph)
                        interface.l = np.linalg.norm(interface.node_coords[0] - interface.node_coords[1])
                        if interface.l > 1.0 * interface.l0 or interface.l < 0.1 * interface.l0:
                            interface.active = False
                        else:
                            interface.active = True

                print("dt = " + str(dt) + " | t = " + str(t) + " | df_t: " + str(np.linalg.norm(df)) + " | vmax: " + str(np.max(np.abs(v))) + " | df_1: " + str(self.convergence * np.linalg.norm(df_1)))
                M, K, dt = self.construct_matrices(E, nu)

                for i in range(6 * len(self.element_list)):
                    element_number = floor(i/6)
                    
                    if self.element_list[element_number].constraints[i%6]:
                            df[i] = 0.0
                    elif abs(v[i]) > 0.5 or v_limit[floor(i/6)]:
                        df[6*element_number:6*element_number+6] = 0.0
                        v_limit[floor(i/6)] = True
                    elif K[i,:].dot(u) == 0:
                        df[i] = 0.0
                    else:
                        internal_force = K[i,:].dot(u)
                        df[i] = f[i] - internal_force

                if loop == 0:
                    df_1 = deepcopy(df)
                    file = open(self.directory_path + "convergence.csv", "a")
                    file.write("1.0")
                    file.close()
                else:
                    file = open(self.directory_path + "convergence.csv", "a")
                    file.write("," + str(np.linalg.norm(df)/np.linalg.norm(df_1)))
                    file.close()

                t += dt
                loop += 1

                if np.linalg.norm(df) < (self.convergence * np.linalg.norm(df_1)) or loop == self.iteration_count:
                    M, K, dt = self.construct_matrices(E, nu, save=True)

                    file = open(self.directory_path + "displacements.dat", "a")

                    for i in range(len(self.element_list)):

                        for j in range(6):
                            file.write("{:.10f}".format(u[6*i + j]) + " ")
                        file.write(str(self.element_list[i].weight) +"\n")

                    file.close()

                    self.u = u
                    self.v = v
                    break

    def assemble_K(self, E, nu, save=False):
        A = (0.25 ** 2) * (self.spacing ** 2) * (2 ** 0.5)

        K_rows = []
        K_cols = []
        K_values = []

        for interface in self.interface_list:
            if interface.active:
                l0 = interface.l0

                k_n = E*A/l0
                k_s = k_n/(2*(nu + 1))

                T = self.transformation_matrix(interface.direction)
                
                element0 = interface.attached_elements[0]
                element1 = interface.attached_elements[1]
                ri_global = interface.center_of_mass - element0.center_of_mass
                rj_global = interface.center_of_mass - element1.center_of_mass
                ri = np.matmul(T[0:3,0:3], ri_global)
                rj = np.matmul(T[0:3,0:3], rj_global)

                K_ele = np.zeros((12,12))

                K_ele[0,0] = K_ele[6,6] = k_n
                K_ele[1,1] = K_ele[2,2] = K_ele[7,7] = K_ele[8,8] = k_s
                K_ele[3,3] = k_s * ri[1] ** 2 + k_s * ri[2] ** 2
                K_ele[4,4] = k_s * ri[0] ** 2 + k_n * ri[2] ** 2
                K_ele[5,5] = k_s * ri[0] ** 2 + k_n * ri[1] ** 2
                K_ele[0,4] = K_ele[4,0] = k_n * ri[2]
                K_ele[0,5] = K_ele[5,0] = -k_n * ri[1]
                K_ele[1,3] = K_ele[3,1] = -k_s * ri[2]
                K_ele[1,5] = K_ele[5,1] = k_s * ri[0]
                K_ele[2,3] = K_ele[3,2] = k_s * ri[1]
                K_ele[2,4] = K_ele[4,2] = -k_s * ri[0]
                K_ele[3,4] = K_ele[4,3] = -k_s * ri[0] * ri[1]
                K_ele[3,5] = K_ele[5,3] = -k_s * ri[0] * ri[2]
                K_ele[4,5] = K_ele[5,4] = -k_n * ri[1] * ri[2]
                
                K_ele[0,6] = K_ele[6,0] = -k_n
                K_ele[1,7] = K_ele[2,8] = K_ele[7,1] = K_ele[8,2] = -k_s
                K_ele[3,9] = K_ele[9,3] = -k_s * ri[1] *  rj[1] - k_s * ri[2] * rj[2]
                K_ele[4,10] = K_ele[10,4] = -k_s * ri[0] * rj[0] - k_n * ri[2] * rj[2]
                K_ele[5,11] = K_ele[11,5] = -k_s * ri[0] * rj[0] - k_n * ri[1] * rj[1]
                K_ele[0,10] = K_ele[10,0] = -k_n * rj[2]
                K_ele[0,11] = K_ele[11,0] = k_n * rj[1]
                K_ele[1,9] = K_ele[9,1] = k_s * rj[2]
                K_ele[1,11] = K_ele[11,1] = -k_s * rj[0]
                K_ele[2,9] = K_ele[9,2] = -k_s * rj[1]
                K_ele[2,10] = K_ele[10,2] = k_s * rj[0]
                K_ele[3,7] = K_ele[7,3] = k_s * ri[2]
                K_ele[3,8] = K_ele[8,3] = -k_s * ri[1]
                K_ele[3,10] = K_ele[10,3] = k_s * ri[1] * rj[0]
                K_ele[3,11] = K_ele[11,3] = k_s * ri[2] * rj[0]
                K_ele[4,6] = K_ele[6,4] = -k_n * ri[2]
                K_ele[4,8] = K_ele[8,4] = k_s * ri[0]
                K_ele[4,9] = K_ele[9,4] = k_s * ri[0] * rj[1]
                K_ele[4,11] = K_ele[11,4] = k_n * ri[2] * rj[1]
                K_ele[5,6] = K_ele[6,5] = k_n * ri[1]
                K_ele[5,7] = K_ele[7,5] = -k_s * ri[0]
                K_ele[5,9] = K_ele[9,5] = k_s * ri[0] * rj[2]
                K_ele[5,10] = K_ele[10,5] = k_n * ri[1] * rj[2]
                
                K_ele[9,9] = k_s * rj[1] ** 2 + k_s * rj[2] ** 2
                K_ele[10,10] = k_s * rj[0] ** 2 + k_n * rj[2] ** 2
                K_ele[11,11] = k_s * rj[0] ** 2 + k_n * rj[1] ** 2
                K_ele[6,10] = K_ele[10,6] = k_n * rj[2]
                K_ele[6,11] = K_ele[11,6] = -k_n * rj[1]
                K_ele[7,9] = K_ele[9,7] = -k_s * rj[2]
                K_ele[7,11] = K_ele[11,7] = k_s * rj[0]
                K_ele[8,9] = K_ele[9,8] = k_s * rj[1]
                K_ele[8,10] = K_ele[10,8] = -k_s * rj[0]
                K_ele[9,10] = K_ele[10,9] = -k_s * rj[0] * rj[1]
                K_ele[9,11] = K_ele[11,9] = -k_s * rj[0] * rj[2]
                K_ele[10,11] = K_ele[11,10] = -k_n * rj[1] * rj[2]

                K_ele_global_unweighted = np.matmul(np.matmul(np.transpose(T), K_ele), T)
                K_ele_global = element0.weight ** self.p * element1.weight ** self.p * K_ele_global_unweighted
                
                if save:
                    element0.save_stiffness_matrix(K_ele_global_unweighted, element1.id, element1.weight)
                    element1.save_stiffness_matrix(K_ele_global_unweighted, element0.id, element0.weight)

                u = element0.id
                v = element1.id
                K_quadrants = ((u,u), (u,v), (v,u), (v,v))

                for i in range(4):
                    for j in range(6):
                        for k in range(6):
                            if i == 0:
                                if element0.constraints[j] or element0.constraints[k]:
                                    continue
                            elif i == 1:
                                if element0.constraints[j] or element1.constraints[k]:
                                    continue
                            elif i == 2:
                                if element1.constraints[j] or element0.constraints[k]:
                                    continue
                            else:
                                if element1.constraints[j] or element1.constraints[k]:
                                    continue

                            K_rows.append(6*K_quadrants[i][0]+j)
                            K_cols.append(6*K_quadrants[i][1]+k)
                            K_values.append(K_ele_global[j+6*floor(i/2), k+6*(i%2)])

        K = sparse.coo_matrix((K_values, (K_rows, K_cols)), shape=(6*len(self.element_list), 6*len(self.element_list))).tocsc()
        return K


    def construct_matrices(self, E, nu, save=False):
        M_rows = []
        M_cols = []
        M_values = []
    
        for element in self.element_list:
            for i in range(6):
                M_rows.append(6*element.id + i)
                M_cols.append(6*element.id + i)
                M_values.append(element.mass_matrix[i,i])

        K = self.assemble_K(E, nu, save=save)
        M = sparse.coo_matrix((M_values, (M_rows, M_cols)), shape=(6*len(self.element_list), 6*len(self.element_list))).tocsc()

        m_over_k = np.divide(M.diagonal(), (K.diagonal() + 1))
        dt = min(0.01, 0.8*(np.min(m_over_k))**0.5)

        return M, K, dt

    def transformation_matrix(self, direction):
        T = np.zeros((12,12))
        
        for i in range(3):
            for j in range(3):
                T[i,j] = T[i+3,j+3] = T[i+6,j+6] = T[i+9,j+9] = direction[i][j]

        return T

    def check_roof(self, x, n, mu):
        q = 3.0
        g = 0.0
        dgdx = np.zeros(n)

        struct_x = 2*self.structure_size[0]
        struct_y = 2*self.structure_size[1]
        struct_z = 2*self.structure_size[2]

        layer_shift = struct_x * struct_y

        for i in range(n):
            for row in range(struct_y):
                for column in range(struct_x):
                    dependent_on_i = False
                    roof_bottom_index = column

                    l = 1.0 - 1.0/q - self.element_list[i].weight + self.element_list[i].weight ** q / q
                    m = 1.0

                    for k in range(struct_z):
                        roof_index = roof_bottom_index + k*layer_shift
                        for subindex in range(8):
                            if type(self.graph.node_list[roof_index][subindex]).__bases__[0] == Graph:
                                m -= x[self.graph.node_list[roof_index][subindex].id] ** (q - 1.0)
                                if self.graph.node_list[roof_index][subindex].id == i:
                                    dependent_on_i = True
                                else:
                                    l += self.element_list[i].weight * x[self.graph.node_list[roof_index][subindex].id] ** q

                    if dependent_on_i:
                        g += l
                        dgdx[i] = min(-m, 0.0)

        return mu*g, mu*dgdx

    def show_3d_graph(self, iteration):
        #we  need to create lists that contain the starting and ending coordinates of each edge.
        x_edges=[]
        y_edges=[]
        z_edges=[]

        x_nodes = {}
        y_nodes = {}
        z_nodes = {}

        nodes_int = []
        node_labels = []

        for i, element in enumerate(self.element_list):
            for j, subcube in enumerate(element.node_coords):
                global_subcube_index = element.get_global_subcube_index(j, self.graph)
                for node_coords in subcube.items():
                    
                    x_nodes[8*global_subcube_index + node_coords[0]] = node_coords[1][0]
                    y_nodes[8*global_subcube_index + node_coords[0]] = node_coords[1][1]
                    z_nodes[8*global_subcube_index + node_coords[0]] = node_coords[1][2]

                    nodes_int.append(element.weight)
                    node_labels.append("node index: " + str(8*global_subcube_index + node_coords[0]) + " | element: " + str(i))

            for edge in element.edge_list:
                global_edge_subcube_indeces = (element.get_global_subcube_index(edge[0][0], self.graph), element.get_global_subcube_index(edge[1][0], self.graph))
                # print(x_nodes, edge, global_edge_subcube_indeces)

                x_coords = [x_nodes[8*global_edge_subcube_indeces[0] + edge[0][1]], x_nodes[8* global_edge_subcube_indeces[1] + edge[1][1]],None]
                x_edges += x_coords

                y_coords = [y_nodes[8*global_edge_subcube_indeces[0] + edge[0][1]], y_nodes[8* global_edge_subcube_indeces[1] + edge[1][1]],None]
                y_edges += y_coords

                z_coords = [z_nodes[8*global_edge_subcube_indeces[0] + edge[0][1]], z_nodes[8* global_edge_subcube_indeces[1] + edge[1][1]],None]
                z_edges += z_coords
        nodes_int.append(0.0)
        nodes_int.append(1.0)

        #need to fill these with all of the coordinates
        x_interfaces=[]
        y_interfaces=[]
        z_interfaces=[]

        for interface in self.interface_list:
            if interface.active:
                #format: [beginning,ending,None]
                x_coords = [x_nodes[8*interface.subcube + interface.node_list[0]], x_nodes[8*interface.subcube + interface.node_list[1]],None]
                x_interfaces += x_coords

                y_coords = [y_nodes[8*interface.subcube + interface.node_list[0]], y_nodes[8*interface.subcube + interface.node_list[1]],None]
                y_interfaces += y_coords

                z_coords = [z_nodes[8*interface.subcube + interface.node_list[0]], z_nodes[8*interface.subcube + interface.node_list[1]],None]
                z_interfaces += z_coords

        #create a trace for the edges
        trace_edges = go.Scatter3d(x=x_edges,
                                y=y_edges,
                                z=z_edges,
                                mode='lines',
                                line=dict(color='black', width=2),
                                hoverinfo="none")

        #create a trace for the edges
        trace_interfaces = go.Scatter3d(x=x_interfaces,
                                y=y_interfaces,
                                z=z_interfaces,
                                mode='lines',
                                line=dict(color='red', width=2),
                                hoverinfo="none")

        trace_nodes = go.Scatter3d(x=list(x_nodes.values()),
                                y=list(y_nodes.values()),
                                z=list(z_nodes.values()),
                                mode='markers',
                                marker=dict(symbol='circle',
                                            size=6,
                                            color=nodes_int,
                                            colorscale=[[0.0,'red'], [1.0,'green']]),
                                text=node_labels,
                                hoverinfo='text')

        #we need to set the axis for the plot 

        layout = go.Layout(title="Graph representing the masonry elements",
                        width=1920,
                        height=1080,
                        showlegend=False,
                        scene=dict(xaxis=dict(  backgroundcolor="white",
                                                gridcolor="rgb(200,200,200)",
                                                title='x'),
                                yaxis=dict(     backgroundcolor="white",
                                                gridcolor="rgb(200,200,200)",
                                                title='y'),
                                zaxis=dict(     backgroundcolor="white",
                                                gridcolor="rgb(200,200,200)",
                                                title='z'),
                                camera=dict(
                                    projection=dict(
                                        type="orthographic"
                                    )
                                )),
                        margin=dict(t=80),
                        hovermode='closest',
                    )

        data = [trace_edges, trace_interfaces, trace_nodes]
        fig = go.Figure(data=data, layout=layout)
        # fig.write_image(self.directory_path + "plotly/" + iteration + ".png")
        fig.show()
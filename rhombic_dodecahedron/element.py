import numpy as np
from math import floor, cos, sin

from rhombic_dodecahedron.graph import Graph

class Element(Graph):
    def __init__(self, element, spacing, f, constraints, applied_forces, graph):
        self.id = 0
        self.weight = f
        self.graph_size = element.graph_size
        self.location = element.location
        self.node_list = element.node_list
        self.edge_list = element.edge_list
        self.spacing = spacing
        self.constraints = constraints
        self.applied_forces = applied_forces
        self.velocities = np.array([0.0] * 6)
        self.stiffness_matrices = {}
        self.other_ids = []
        self.other_weights = []

        self.node_coords = self.get_nodes_info(graph)

        self.mass_density = 2000.0
        self.calculate_center_of_mass()
        self.calculate_moment_of_inertia()
        self.mass_matrix = np.zeros((6,6))
        self.mass_matrix[0, 0] = self.mass_matrix[1, 1] = self.mass_matrix[2, 2] = self.mass
        self.mass_matrix[3:6, 3:6] = self.I

    def get_nodes_info(self, graph):
        node_coords = []

        for i in range(self.graph_size[0] * self.graph_size[1] * self.graph_size[2]):
            node_coords.append({})
            for j in range(8):
                if  self.node_list[i][j] != True:
                    node_coords[i][j] = self.calculate_global_coordinates(i, j, graph)

        return node_coords

    def get_global_subcube_index(self, subcube, graph):
        loc = self.location
        global_coords = [-1, -1, -1]

        global_coords[0] = (subcube % self.graph_size[0])
        global_coords[1] = floor(subcube / self.graph_size[0]) % self.graph_size[1] * graph.graph_size[0]
        global_coords[2] = floor(subcube / (self.graph_size[0] * self.graph_size[1])) * (graph.graph_size[0] * graph.graph_size[1])

        return loc + sum(global_coords)

    def get_subcube_chirality(self, global_index, graph):
        #chirality works as follows:
        #   0: index 0 has location (0.375, 0.125, 0.125) for subcube for unit RD
        #   1: index 0 has location (0.125, 0.125, 0.125) for subcube for unit RD
        chirality = (global_index + floor(global_index/graph.graph_size[0]) + floor(global_index/(graph.graph_size[0]*graph.graph_size[1])))%2

        return chirality

    def get_local_index(self, global_subcube_index, graph):
        dz = floor((global_subcube_index - self.location)/(graph.graph_size[0] * graph.graph_size[1]))
        dy = floor(((global_subcube_index - self.location)%(graph.graph_size[0] * graph.graph_size[1]))/graph.graph_size[0])
        dx = ((global_subcube_index - self.location)%(graph.graph_size[0] * graph.graph_size[1]))%graph.graph_size[0]

        return dx + dy * self.graph_size[0] + dz * self.graph_size[0] * self.graph_size[1]


    def calculate_global_coordinates(self, subcube, subindex, graph):
        #get global subcube index and chirality from local subcube index, element location and global graph
        global_index = self.get_global_subcube_index(subcube, graph)
        chirality = self.get_subcube_chirality(global_index, graph)

        subindex_locations = \
                            (
                                (
                                    (0.375, 0.125, 0.125), (0.3125, 0.1875, 0.1875), (0.125, 0.375, 0.125), (0.1875, 0.3125, 0.1875),
                                    (0.125, 0.125, 0.375), (0.1875, 0.1875, 0.3125), (0.375, 0.375, 0.375), (0.3125, 0.3125, 0.3125)
                                ),
                                (
                                    (0.125, 0.125, 0.125), (0.1875, 0.1875, 0.1875), (0.375, 0.375, 0.125), (0.3125, 0.3125, 0.1875),
                                    (0.375, 0.125, 0.375), (0.3125, 0.1875, 0.3125), (0.125, 0.375, 0.375), (0.1875, 0.3125, 0.3125)
                                ),
                            )

        #get (x,y,z) coordinates from global subcube indeces, subindeces and RD spacing
        x = self.spacing * (global_index % graph.graph_size[0]/2 + subindex_locations[chirality][subindex][0]) 
        y = self.spacing * ((floor(global_index / graph.graph_size[0]) % graph.graph_size[1]) / 2 + subindex_locations[chirality][subindex][1])
        z = self.spacing * ((floor(global_index / (graph.graph_size[0] * graph.graph_size[1])) % graph.graph_size[2])/2 + subindex_locations[chirality][subindex][2])

        return np.array((float(x),float(y),float(z)))

    def calculate_center_of_mass(self):
        center_of_mass = [0,0,0]
        n_nodes = 0

        for subcube in self.node_coords:
            for node_coords in subcube.values():
                center_of_mass[0] += node_coords[0]
                center_of_mass[1] += node_coords[1]
                center_of_mass[2] += node_coords[2]
                n_nodes += 1

        for i in range(3):
            center_of_mass[i] = center_of_mass[i] / n_nodes

        self.center_of_mass = np.array(center_of_mass)
        self.mass = 0.25 * (self.spacing ** 3.0) * self.mass_density

    def calculate_moment_of_inertia(self):
        I = np.zeros((3, 3))

        #value is calculated for a unit RD and scaled by the element spacing and mass density
        I[0,0] = I[1,1] = I[2,2] = (self.spacing ** 5.0) * self.mass_density * 0.00295139

        self.I = I

    def translate_element(self, u):
        self.center_of_mass += u
        for i, subcube in enumerate(self.node_coords):
            for node in subcube.keys():
                self.node_coords[i][node] += u

    def rotate_element(self, phi):
        g = self.center_of_mass
        
        Rx = np.zeros((3,3))
        Ry = np.zeros((3,3))
        Rz = np.zeros((3,3))

        Rx[0,0] = 1.0
        Rx[1,1] = Rx[2,2] = cos(phi[0])
        Rx[1,2] = -sin(phi[0])
        Rx[2,1] = sin(phi[0])

        Ry[0,0] = Ry[2,2] = cos(phi[1])
        Ry[1,1] = 1.0
        Ry[0,2] = sin(phi[1])
        Ry[2,0] = -sin(phi[1])

        Rz[0,0] = Rz[1,1] = cos(phi[2])
        Rz[2,2] = 1.0
        Rz[0,1] = -sin(phi[2])
        Rz[1,0] = sin(phi[2])

        rotations = (Rx, Ry, Rz)

        for i, subcube in enumerate(self.node_coords):
            for node in subcube.keys():
                for R in rotations:
                    s = self.node_coords[i][node]
                    d = s - g
                    self.node_coords[i][node] = np.matmul(R, d) + g

    def save_stiffness_matrix(self, matrix, other_id, other_weight):
        if other_id in self.stiffness_matrices:
            self.stiffness_matrices[other_id] += matrix
        else:
            self.stiffness_matrices[other_id] = matrix
            self.other_ids.append(other_id)
            self.other_weights.append(other_weight)
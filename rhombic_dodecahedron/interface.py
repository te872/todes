from copy import deepcopy

import numpy as np

class Interface():
    def __init__(self, subcube, subindeces, spacing, graph, length):
        self.active = True
        self.subcube = subcube
        self.node_list = subindeces
        self.spacing = spacing
        self.l = length
        self.l0 = length
        self.attached_elements = (graph.node_list[self.subcube][self.node_list[0]], graph.node_list[self.subcube][self.node_list[1]])
        self.center_of_mass, self.node_coords = self.calculate_center_of_mass(graph)
        self.p0 = deepcopy(self.node_coords)
        self.get_vectors()

    def update_node_coords(self, graph):
        node_coords = []

        for i, node in enumerate(self.node_list):
            element = self.attached_elements[i]
            local_subcube_index = element.get_local_index(self.subcube, graph)
            coords = element.node_coords[local_subcube_index][node]
            node_coords.append(coords)

        return node_coords

    def calculate_center_of_mass(self, graph):
        node_coords = self.update_node_coords(graph)

        return(node_coords[0] + node_coords[1])/2, node_coords

    def get_perpendicular_vectors(self, v):
        v1 = np.cross(v, [1,0,0])

        if np.linalg.norm(v1) > 0.0:
            v1 = v1 / np.linalg.norm(v1)
        else:
            v1 = np.cross(v, [0,1,0])
            v1 = v1 / np.linalg.norm(v1)
        
        v2 = np.cross(v, v1)
        return (v1, v2)

    def get_vectors(self):
        self.forces = []
        
        normal_vector = np.array(self.node_coords[0]) - np.array(self.center_of_mass)
        normal_vector = normal_vector / np.linalg.norm(normal_vector)
        friction1, friction2 = self.get_perpendicular_vectors(normal_vector)
        self.direction = (normal_vector, friction1, friction2)

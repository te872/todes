import numpy as np

tetrahedrons = []
scale = 1.0

for i in range(8):
    for j in range(2):
        verteces = []

        if j > 0:
            s = 0.25
        else:
            s = 0.0

        bin = "{:03b}".format(i)
        a = scale*(2*int(bin[0]) - 1)
        b = scale*(2*int(bin[1]) - 1)
        c = scale*(2*int(bin[2]) - 1)

        verteces.append(np.array([a*s,b*s,c*s]))
        verteces.append(np.array([a*0.5,0.0,0.0]))
        verteces.append(np.array([0.0,b*0.5,0.0]))
        verteces.append(np.array([0.0,0.0,c*0.5]))

        tetrahedrons.append(verteces)

V_total = 0.0
I = np.zeros((3,3))

for tetrahedron in tetrahedrons:
    T = np.ones((4,4))

    for i in range(4):
        T[i,0:3] = tetrahedron[i]

    V = abs(np.linalg.det(T))/6
    V_total += V

    for i in range(4):
        for j in range(4):
            I[0,0] += V*(tetrahedron[i][1] * tetrahedron[j][1] + tetrahedron[i][2] * tetrahedron[j][2])/60
            I[1,1] += V*(tetrahedron[i][0] * tetrahedron[j][0] + tetrahedron[i][2] * tetrahedron[j][2])/60
            I[2,2] += V*(tetrahedron[i][1] * tetrahedron[j][1] + tetrahedron[i][1] * tetrahedron[j][1])/60

            I[1,0] -= V*(tetrahedron[i][0] * tetrahedron[j][2])/120
            I[2,0] -= V*(tetrahedron[i][0] * tetrahedron[j][1])/120
            I[2,1] -= V*(tetrahedron[i][1] * tetrahedron[j][2])/120

        I[1,0] -= V*(tetrahedron[i][0] * tetrahedron[j][2])/120
        I[2,0] -= V*(tetrahedron[i][0] * tetrahedron[j][1])/120
        I[2,1] -= V*(tetrahedron[i][1] * tetrahedron[j][2])/120

I[0,1] = I[1,0]
I[0,2] = I[2,0]
I[1,2] = I[1,2]

print(round(V_total,10))
print(np.around(I,10))
from math import floor

class Graph():
    def __init__(self, size):
        x_size = size[0]
        y_size = size[1]
        z_size = size[2]

        self.graph_size = (x_size, y_size, z_size)
        self.node_list = []
        self.edge_list = []

    #populate node list with True values (True = free empty node)
    def create_graph(self):
        node_count = self.graph_size[0] * self.graph_size[1] * self.graph_size[2]

        for i in range(node_count):
            self.node_list.append([True] * 8)

    #set void indeces to False
    def set_void(self, location, size):
        n_columns = self.graph_size[0]
        n_rows = self.graph_size[1]
        
        for layer in range(size[2]):
            for row in range(size[1]):
                for column in range(size[0]):
                    for i in range(8):
                        self.node_list[location + column + row * n_columns + layer * n_columns * n_rows][i] = False

    def check_availabilities(self, subgraph, offset_direction=0):
        n_columns = self.graph_size[0]
        n_rows = self.graph_size[1]
        n_layers = self.graph_size[2]

        shift = (0, 1, n_columns, n_columns*n_rows)
        loc = subgraph.location + shift[offset_direction]
        global_sc_indeces = []

        #check if the subgraph fits within the total graph
        if loc % n_columns + subgraph.graph_size[0] > n_columns or floor(loc / n_columns) % n_rows + subgraph.graph_size[1] > n_rows or floor(loc / (n_columns * n_rows)) + subgraph.graph_size[2] > n_layers:
            return False

        #get global indeces of the subgraph subcubes
        for layer in range(subgraph.graph_size[2]):
            for row in range(subgraph.graph_size[1]):
                for column in range(subgraph.graph_size[0]):
                    global_sc_indeces.append(loc + column + row * n_columns + layer * n_columns * n_rows)

        #check if the indeces of the to-be-applied subgraph are not occupied or set as a void
        for i in range(len(subgraph.node_list)):
            for j in range(8):
                if type(subgraph.node_list[i][j]).__bases__[0] == Graph and (self.node_list[global_sc_indeces[i]][j] == False or type(self.node_list[global_sc_indeces[i]][j]).__bases__[0] == Graph):
                    return False

        return True

    def apply_subgraph_to_graph(self, subgraph):
        n_columns = self.graph_size[0]
        n_rows = self.graph_size[1]

        loc = subgraph.location
        global_sc_indeces = []

        #convert and add local edges to global edges as: tuple (index0, index1)
        for i in range(len(subgraph.edge_list)):
            v0 = subgraph.edge_list[i][0][1]
            v1 = subgraph.edge_list[i][1][1]
            sc0 = subgraph.edge_list[i][0][0]
            sc1 = subgraph.edge_list[i][1][0]

            global_sc0 = loc + sc0%subgraph.graph_size[0] + floor(sc0/subgraph.graph_size[0]) * n_columns + floor(sc0/(subgraph.graph_size[0]*subgraph.graph_size[1])) * n_columns * n_rows
            global_sc1 = loc + sc1%subgraph.graph_size[0] + floor(sc1/subgraph.graph_size[0]) * n_columns + floor(sc1/(subgraph.graph_size[0]*subgraph.graph_size[1])) * n_columns * n_rows

            global_edges =  ((global_sc0, v0),(global_sc1, v1))

            self.edge_list.append(global_edges)

        #get global indeces of the subgraph subcubes
        for layer in range(subgraph.graph_size[2]):
            for row in range(subgraph.graph_size[1]):
                for column in range(subgraph.graph_size[0]):
                    global_sc_indeces.append(loc + column + row * n_columns + layer * n_columns * n_rows)

        #convert and add local indeces to global indeces
        for i in range(len(subgraph.node_list)):
            for j in range(8):
                if type(subgraph.node_list[i][j]).__bases__[0] == Graph:
                    self.node_list[global_sc_indeces[i]][j] = subgraph
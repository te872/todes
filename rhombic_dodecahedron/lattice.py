from math import floor

from rhombic_dodecahedron.graph import Graph
from rhombic_dodecahedron.element import Element
from rhombic_dodecahedron.whole_element import Whole_element as we

class Lattice():
    def __init__(self, size, supported_elements, applied_forces, spacing, f, void_location, void_size):
        self.size = size
        self.supported_elements = supported_elements
        self.applied_forces = applied_forces
        self.spacing = spacing
        self.graph = Graph((size[0] * 2, size[1] * 2, size[2] * 2))
        self.graph.create_graph()

        for i in range(len(void_location)):
            layer = floor(void_location[i] / (size[0] * size[1]))
            row = floor(void_location[i] / (size[0]))%size[1]
            column = void_location[i]%size[0]
            self.set_void(column, row, layer, void_size[i])

        self.element_list = self.populate_lattice(f)

    def set_void(self, column, row, layer, size):
        location = 2*column + 4*self.size[0]*row + 8*self.size[0]*self.size[1]*layer
        self.graph.set_void(location, (size[0] * 2, size[1] * 2, size[2] * 2))

    def get_graph_index(self, column, row, layer):
        x = 2*self.size[0]
        y = 2*self.size[1]

        location = (column + row*x + layer*x*y)

        return location

    def populate_lattice(self, f):
        x = 2*self.size[0]
        y = 2*self.size[1]
        z = 2*self.size[2]
        element_list = []

        all_fixed = (True, True, True, True, True, True)
        unsupported = (False, False, False, True, True, True)

        no_force = [0,0,0,0,0,0]
        downward_force = [0,0,-100000,0,0,0]

        #insert on-grid whole elements
        for layer in range(0, z-1):
            for row in range(0, y-1):
                column_offset = (layer + row)%2
                for column in range(column_offset, x, 2):

                    element_center = (0.5 * self.spacing * (column + 1), 0.5 * self.spacing * (row + 1), 0.5 * self.spacing * (layer + 1))
                    location = self.get_graph_index(column, row, layer)

                    supports = unsupported
                    force = no_force

                    for ranges in self.supported_elements:
                        if element_center[0] >= ranges[0][0] and element_center[0] <= ranges[0][1] and element_center[1] >= ranges[1][0] and element_center[1] <= ranges[1][1] and element_center[2] >= ranges[2][0] and element_center[2] <= ranges[2][1]:
                            supports = all_fixed
                            break

                    for ranges in self.applied_forces:
                        if element_center[0] >= ranges[0][0] and element_center[0] <= ranges[0][1] and element_center[1] >= ranges[1][0] and element_center[1] <= ranges[1][1] and element_center[2] >= ranges[2][0] and element_center[2] <= ranges[2][1]:
                            force = downward_force
                            break 

                    element = Element(we(location), self.spacing, f, supports, force, self.graph)

                    if self.graph.check_availabilities(element):
                        self.graph.apply_subgraph_to_graph(element)
                        element_list.append(element)

        return element_list
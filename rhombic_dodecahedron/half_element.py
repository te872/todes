from math import floor
from rhombic_dodecahedron.graph import Graph
from rhombic_dodecahedron.quarter_element import Quarter_element

class Half_element(Graph):
    def __init__(self, location, normal, orientation, subcubes=(0,1,2,3)):
        size = [2, 2, 2]
        size[normal] = int(size[normal] / 2)
        super().__init__(size)

        self.normal = normal
        self.location = location

        #orientation:
        #   0 -> RD center facing in positive direction
        #   1 -> RD center facing in negative direction
        self.orientation = orientation
        self.subcubes = subcubes
        
        super().create_graph()
        self.create_graph()

    def create_graph(self):
        #tuple to get the quarter element orientations right for each half element normal and orientation
        #first index: normal
        #second index: orientation
        quarter_info = \
                        (((3,1), (2,0)),
                         ((3,2), (1,0)),
                         ((3,2), (1,0)))

        v = []
        
        for i in range(2):
            quarter_orientation = quarter_info[self.normal][self.orientation][i]
            quarter_axis = (self.normal + 1) % 3

            quarter_subcubes = (self.subcubes[0+2*i], self.subcubes[1+2*i])
            quarter = Quarter_element(0, quarter_orientation, quarter_axis, quarter_subcubes)
            if super().check_availabilities(quarter, i*(1 + (self.normal + 2)%3)) == False:
                self.node_list = []
                self.edge_list = []
                super().create_graph()
                return

            for edge in quarter.edge_list:
                self.edge_list.append(edge)

            for j, sc in enumerate(quarter.node_list):
                for k, node in enumerate(sc):
                    if node != True:
                        if self.normal == 1:
                            self.node_list[2*j + i][k] = self
                        else:
                            self.node_list[2*i + j][k] = self

        if self.normal == 0:
            self.edge_list.append(((self.subcubes[0],6),(self.subcubes[2],2)))
            self.edge_list.append(((self.subcubes[1],4),(self.subcubes[3],0)))
        if self.normal == 1:
            self.edge_list.append(((self.subcubes[0],6-2*self.orientation),(self.subcubes[1],6-2*self.orientation)))
            self.edge_list.append(((self.subcubes[2],2-2*self.orientation),(self.subcubes[3],2-2*self.orientation)))
        if self.normal == 2:
            self.edge_list.append(((self.subcubes[0],6-4*self.orientation),(self.subcubes[2],4-4*self.orientation)))
            self.edge_list.append(((self.subcubes[1],6-4*self.orientation),(self.subcubes[3],4-4*self.orientation)))

    def apply_to_global(self, graph):
        graph.apply_subgraph_to_graph(self)
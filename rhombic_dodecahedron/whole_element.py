from rhombic_dodecahedron.graph import Graph
from rhombic_dodecahedron.half_element import Half_element

class Whole_element(Graph):
    def __init__(self, location):
        super().__init__((2, 2, 2))
        self.location = location
        
        super().create_graph()
        self.create_graph()

    def create_graph(self):
        half_normal = 0

        for i in range(2):
            half_orientation = bool(i)

            half = Half_element(0, half_normal, half_orientation, (0+i,2+i,4+i,6+i))
            if super().check_availabilities(half, 1-i) == False:
                self.node_list = []
                self.edge_list = []
                super().create_graph()
                return

            for edge in half.edge_list:
                self.edge_list.append(edge)

            for j, sc in enumerate(half.node_list):
                for k, node in enumerate(sc):
                    if node != True:
                        self.node_list[2*j + 1 - i][k] = self

        for i in range(4):
            self.edge_list.append(( (2*i, 2*(3-i)) , (2*i+1, 2*(3-i))) )

    def apply_to_global(self, graph):
        graph.apply_subgraph_to_graph(self)
from rhombic_dodecahedron.graph import Graph

class  Molecule(Graph):
    def __init__(self, location, orientation, subcube=0):
        super().__init__((1,1,1))
        super().create_graph()

        #note: location is the smallest index of the subcube which houses the molecule!
        self.location = location

        #orientation works as follows:
        #   0 -> location closest to x-axis (bottom)
        #   1 -> location furthest from x-axis (bottom)
        #   2 -> location closest to x-axis (top)
        #   3 -> location furthest from x-axis (top)
        self.orientation = orientation

        self.subcube = subcube

        self.create_graph()

    #set occupied nodes to self and add edge
    def create_graph(self):
        v0 = 2*self.orientation
        v1 = v0 + 1
        self.node_list[0][v0] = self.node_list[0][v1] = self
        self.edge_list.append(((self.subcube,v0), (self.subcube,v1)))

        if super().check_availabilities(self, 0) == False:
            return
    
    def apply_to_global(self, graph):
        graph.apply_subgraph_to_graph(self)
from cubic_sphere.simulation import Simulation as CS_sim
from rhombic_dodecahedron.simulation import Simulation as RD_sim
import numpy as np
from scipy.sparse import coo_matrix
from MMA import mmasub,subsolv

from copy import deepcopy

class Optimization():
    def __init__(self, e_type, model_type, r, p, f, E, nu):
        self.p = p
        self.f = f
        self.x_min = 0.001
        self.iteration_count = 100

        if e_type == 0:
            self.simulation = CS_sim(model_type, self.p, self.f)
        if e_type == 1:
            self.simulation = RD_sim(model_type, self.p, self.f)

        self.n = len(self.simulation.element_list)
        self.r = r

        x = self.f*np.ones(self.n, dtype=float)
        xPhys = x.copy()

        m = 2
        xmin = np.zeros((self.n,1)) + 0.001
        xmax = np.ones((self.n,1)) 
        xval = x[np.newaxis].T 
        xold1 = xval.copy() 
        xold2 = xval.copy() 
        low = np.ones((self.n,1))
        upp = np.ones((self.n,1))
        a0 = 1.0 
        a = np.zeros((m,1)) 
        c = 10000*np.ones((m,1))
        d = np.zeros((m,1))
        move = 0.2 

        iH = []
        jH = []
        sH = []

        for eli in self.simulation.element_list:
            for elj in self.simulation.element_list:
                if abs(eli.center_of_mass[0] - elj.center_of_mass[0]) > self.r:
                    continue
                elif abs(eli.center_of_mass[1] - elj.center_of_mass[1]) > self.r:
                    continue
                elif abs(eli.center_of_mass[2] - elj.center_of_mass[2]) > self.r:
                    continue

                s = np.linalg.norm(eli.center_of_mass - elj.center_of_mass)
                if s < self.r:
                    iH.append(eli.id)
                    jH.append(elj.id)
                    sH.append(self.r - s)

        H = coo_matrix((sH,(iH,jH)),shape=(self.n, self.n)).tocsc()    
        Hs = H.sum(1)

        loop = 0
        change = 1.0
        dv = np.ones(self.n)
        while (change>0.001) and (loop<self.iteration_count):
            loop = loop+1
            # Setup and solve FE problem
            self.simulation.assemble_system(E, nu)  
            # Objective and sensitivity
            obj, dc = self.compliance(xPhys)
            dv[:] = np.ones(self.n)
            # Sensitivity filtering:
            dc[:] = np.asarray(H*(dc[np.newaxis].T/Hs))[:,0]
            dv[:] = np.asarray(H*(dv[np.newaxis].T/Hs))[:,0]
            # Method of moving asymptotes
            mu0 = 1.0 # Scale factor for objective function
            mu1 = 5.0 # Scale factor for volume constraint function
            mu2 = 1.0 # Scale factor for roof constraint function

            f0val = mu0*obj 
            df0dx = mu0*dc[:,np.newaxis]
            fval = np.array([[mu1*xPhys.sum()/self.n-self.f], [0.0]])
            dfdx = np.zeros((2, self.n))
            dfdx[0,:] = mu1*(dv/(self.n))
            gval, dfdx[1,:] = self.simulation.check_roof(xPhys, self.n, mu2)
            fval[1,0] = gval
            xval = x.copy()[:,np.newaxis]
            xmma,ymma,zmma,lam,xsi,eta,mu,zet,s,low,upp = \
                mmasub(m,self.n,loop,xval,xmin,xmax,xold1,xold2,f0val,df0dx,fval,dfdx,low,upp,a0,a,c,d,move)
            xold2 = xold1.copy()
            xold1 = xval.copy()
            x = xmma.copy().flatten()
            # Filter design variables
            xPhys[:] = np.asarray(H*x[np.newaxis].T/Hs)[:,0]
            # Compute the change by the inf. norm
            change = np.linalg.norm(x.reshape(self.n,1)-xold1.reshape(self.n,1),np.inf)
            # Assign new element weights
            for i, element in enumerate(self.simulation.element_list):
                element.weight = xPhys[i]
            # Write iteration history to screen (req. Python 2.6 or newer)
            print("it.: {0} , obj.: {1:.3f} Vol.: {2:.3f}, ch.: {3:.3f}".format(loop,obj,x.sum()/(self.n),change))

    def compliance(self, x):
        c = 0.0
        dc = []
        E_p = 0.0
        u = deepcopy(self.simulation.u)

        work = np.dot(self.simulation.f, self.simulation.u)

        for i, element in enumerate(self.simulation.element_list):
            dE_p = 0.0
            dwork = -self.p * element.weight ** (self.p - 1) * element.mass * 9.81 * self.simulation.u[6*i+2]

            for j in element.other_ids:

                if i < j:
                    element_u = np.array([0.0] * 12)

                    element_u[0:6] = u[6*i : 6*i + 6]
                    element_u[6:12] = u[6*j : 6*j + 6]
                    
                    E_p += x[i] ** (self.p) * x[j] ** (self.p) * np.matmul(np.matmul(np.transpose(element_u), element.stiffness_matrices[j]), element_u)
                
                else:
                    element_u[0:6] = u[6*j : 6*j + 6]
                    element_u[6:12] = u[6*i : 6*i + 6]

                dE_p += self.p *x[i] ** (self.p - 1) * x[j] ** (self.p) * np.matmul(np.matmul(np.transpose(element_u), element.stiffness_matrices[j]), element_u)
           
            dc.append(dwork - dE_p)

        c = work - E_p
    
        return c, np.array(dc)

#element types:
#   0 -> cubic sphere
#   1 -> rhombic dodecahedron
element_type = 0

#model types:
#   0 -> time-independent
#   1 -> time-dependent with global damping
#   2 -> time-dependent with local damping
model_type = 0

#set radius for filtering as a multiple of the spacing between elements
r = 1.6

#set penalization
p = 3

#set desired volume fraction of the optimized structure
f = 0.4

#set Young's modulus
E = 2000.0e+6

#set Poisson's ratio
nu = 0.25

Optimization(element_type, model_type, r, p, f, E, nu)
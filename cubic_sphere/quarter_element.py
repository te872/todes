from math import floor

from cubic_sphere.graph import Graph
from cubic_sphere.molecule import Molecule

class Quarter_element(Graph):
    v1_list = ((1, 5, 9, 13), (2, 3, 10, 11), (4, 5, 6, 7))

    def __init__(self, location, axis, orientation):
        sizes = [2, 2, 2]
        sizes[axis] = 2 * sizes[axis]

        self.graph_size = tuple(sizes)
        self.location = location
        self.axis = axis
        self.orientation = orientation
        
        super().create_graph(False)
        self.create_graph()

    def create_graph(self):
        for i in range(2):
            molecule_orientation = [True, True, True]
            molecule_orientation[self.axis] = bool(i)

            for j in range(len(molecule_orientation)):
                if self.axis != j:
                    if j > self.axis:
                        molecule_orientation[j] = self.orientation[j-1]
                    else:
                        molecule_orientation[j] = self.orientation[j]

            molecule_location = (2 * i) ** (self.axis + 1)

            molecule = Molecule(molecule_location, molecule_orientation)
            super().apply_subgraph_to_graph(molecule)

        for i in range(4):
            v1 = 2 ** self.axis

            if self.axis == 1:
                v1 += i % 2 + floor(i/2) * 8
            else:
                v1 += i * (2 ** abs(self.axis - 2))
            
            v2 = v1 + (2 ** self.axis)

            if type(self.node_list[v1]).__bases__[0] == Graph and type(self.node_list[v2]).__bases__[0] == Graph:
                self.edge_list.append((v1, v2))
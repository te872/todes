from math import floor

from cubic_sphere.graph import Graph
from cubic_sphere.element import Element
from cubic_sphere.whole_element import Whole_element as we

class Voxel_lattice():
    def __init__(self, size, supported_elements, applied_forces, spacing, f, void_location, void_size):
        self.size = size
        self.supported_elements = supported_elements
        self.applied_forces = applied_forces
        self.spacing = spacing
        self.graph = Graph((size[0] * 4, size[1] * 4, size[2] * 4))
        self.graph.create_graph(True)

        for i in range(len(void_location)):
            self.set_void(void_location[i], void_size[i])
        
        self.element_list = self.populate_lattice(f)

    def set_void(self, index, size):
        location = self.get_graph_index(index)
        self.graph.set_void(location, (size[0] * 4, size[1] * 4, size[2] * 4))

    def get_graph_index(self, index):
        x = self.size[0]
        y = self.size[1]

        location = 4 * (index%x) + 16 * x * floor((index%(x*y))/x) + 64 * x * y * floor(index/(x*y))

        return location

    def populate_lattice(self, f):
        x = self.size[0]
        y = self.size[1]
        z = self.size[2]
        element_list = []

        all_fixed = (True, True, True, True, True, True)
        unsupported = (False, False, False, False, False, False)

        no_force = [0,0,0,0,0,0]
        downward_force = [0,0,-100000,0,0,0]

        #insert on-grid whole elements
        for i in range(x*y*z):

            location = self.get_graph_index(i)
            element_center = (self.spacing * (4*(i%x) + 2), self.spacing * (4*(floor(i/x)%y) + 2), self.spacing * (4*floor(i/(x*y)) + 2))
            supports = unsupported
            force = no_force
            
            for ranges in self.supported_elements:
                if element_center[0] >= ranges[0][0] and element_center[0] <= ranges[0][1] and element_center[1] >= ranges[1][0] and element_center[1] <= ranges[1][1] and element_center[2] >= ranges[2][0] and element_center[2] <= ranges[2][1]:
                    supports = all_fixed
                    break

            for ranges in self.applied_forces:
                if element_center[0] >= ranges[0][0] and element_center[0] <= ranges[0][1] and element_center[1] >= ranges[1][0] and element_center[1] <= ranges[1][1] and element_center[2] >= ranges[2][0] and element_center[2] <= ranges[2][1]:
                    force = downward_force
                    
                    break   

            element = Element(we(location), self.spacing, f, supports, force, self.graph)

            if self.graph.apply_subgraph_to_graph(element):
                element_list.append(element)

        #insert off-grid whole elements
        for i in range((x - 1) * (y - 1) * (z - 1)):
            macro_index = i%(x - 1) + x*(floor(i/(x - 1))%(y - 1)) + x*y*floor(i/((x - 1)*(y - 1)))
            location = self.get_graph_index(macro_index) + 2*(1+4*x+16*x*y)
            element_center = (self.spacing * (4*(i%(x - 1)) + 4), self.spacing * (4*floor(i/(x - 1)) + 4), self.spacing * (4*floor(i/((x - 1)*(y - 1))) + 4))
            
            supports = unsupported
            force = no_force

            for ranges in self.supported_elements:
                if element_center[0] >= ranges[0][0] and element_center[0] <= ranges[0][1] and element_center[1] >= ranges[1][0] and element_center[1] <= ranges[1][1] and element_center[2] >= ranges[2][0] and element_center[2] <= ranges[2][1]:
                    supports = all_fixed
                    break

            for ranges in self.applied_forces:
                if element_center[0] >= ranges[0][0] and element_center[0] <= ranges[0][1] and element_center[1] >= ranges[1][0] and element_center[1] <= ranges[1][1] and element_center[2] >= ranges[2][0] and element_center[2] <= ranges[2][1]:
                    force = downward_force
                    break   

            element = Element(we(location), self.spacing, f, supports, force, self.graph)

            if self.graph.apply_subgraph_to_graph(element):
                element_list.append(element)

        return element_list
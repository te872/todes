from math import floor

class Graph():
    def __init__(self, size):
        x_size = size[0]
        y_size = size[1]
        z_size = size[2]

        self.graph_size = (x_size, y_size, z_size)

    def create_graph(self, initial_state):
        self.node_list = []
        self.edge_list = []

        node_count = self.graph_size[0] * self.graph_size[1] * self.graph_size[2]

        for i in range(node_count):
            self.node_list.append(True)

    def set_void(self, location, size):
        for i in range(size[2]):
            for j in range(size[1]):
                for k in range(size[0]):
                    self.node_list[location + k + j * self.graph_size[0] + i * self.graph_size[0] * self.graph_size[1]] = False

    def apply_subgraph_to_graph(self, subgraph):
        loc = subgraph.location

        if loc % self.graph_size[0] + subgraph.graph_size[0] > self.graph_size[0] or floor(loc / self.graph_size[0]) % self.graph_size[1] + subgraph.graph_size[1]> self.graph_size[1] or floor(loc / (self.graph_size[0] * self.graph_size[1])) + subgraph.graph_size[2] > self.graph_size[2]:
            # print("WARNING: Sub-graph does not fit in graph!")
            return False

        super_node_indeces = []

        for i in range(subgraph.graph_size[2]):
            for j in range(subgraph.graph_size[1]):
                for k in range(subgraph.graph_size[0]):
                    super_node_indeces.append(loc + k + j * self.graph_size[0] + i * self.graph_size[0] * self.graph_size[1])

        for i in range(len(subgraph.node_list)):
            if (self.node_list[super_node_indeces[i]] == False or type(self.node_list[super_node_indeces[i]]).__bases__[0] == Graph) and type(subgraph.node_list[i]).__bases__[0] == Graph:
                # print("WARNING: An element is already occupying this location")
                return False

        for i in range(len(subgraph.edge_list)):
            v1 = subgraph.edge_list[i][0]
            v2 = subgraph.edge_list[i][1]
            super_edges =  (loc + v1 % subgraph.graph_size[0] + floor(v1 / subgraph.graph_size[0]) % subgraph.graph_size[1] * self.graph_size[0] + floor(v1 / (subgraph.graph_size[0] * subgraph.graph_size[1])) * self.graph_size[0] * self.graph_size[1],
                            loc + v2 % subgraph.graph_size[0] + floor(v2 / subgraph.graph_size[0]) % subgraph.graph_size[1] * self.graph_size[0] + floor(v2 / (subgraph.graph_size[0] * subgraph.graph_size[1])) * self.graph_size[0] * self.graph_size[1])

            self.edge_list.append(super_edges)

        for i in range(len(subgraph.node_list)):
            if type(subgraph.node_list[i]).__bases__[0] == Graph:
                self.node_list[super_node_indeces[i]] = subgraph
        
        return True
from math import floor
from cubic_sphere.graph import Graph
from cubic_sphere.quarter_element import Quarter_element

class Half_element(Graph):
    def __init__(self, location, normal, orientation):
        sizes = [4, 4, 4]
        sizes[normal] = int(sizes[normal] / 2)

        self.graph_size = tuple(sizes)
        self.location = location
        self.normal = normal
        self.orientation = orientation
        
        super().create_graph(False)
        self.create_graph()

    def create_graph(self):
        for i in range(2):
            quarter_axis = (self.normal + 1) % 3

            quarter_orientation = [True, True]
            quarter_orientation[1 - min(1, self.normal)] = bool(i)
            quarter_orientation[min(1, self.normal)] = self.orientation

            quarter_location = (2 * i) ** ((self.normal + 2) % 3 + 1) * ((self.normal + 1) % 2 + 1)

            quarter = Quarter_element(quarter_location, quarter_axis, quarter_orientation)
            super().apply_subgraph_to_graph(quarter)

        for i in range(8):
            # x -> 8 9 10 11 12 13 14 15
            # y -> 1 5 9 13 17 21 25 29
            # z -> 4 5 6 7 20 21 22 23
            v1 = 2 ** ((self.normal + 2) % 3 - self.normal % 2 + 1)
            v1 += i * (4 ** (self.normal % 2))

            if self.normal == 2:
                v1 += floor(i / 4) * 12
            
            v2 = v1 + (2 ** ((self.normal + 2) % 3 - self.normal % 2 + 1))

            if type(self.node_list[v1]).__bases__[0] == Graph and type(self.node_list[v2]).__bases__[0] == Graph:
                self.edge_list.append((v1, v2))
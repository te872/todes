from cubic_sphere.graph import Graph
from math import floor

class  Molecule(Graph):
    def __init__(self, location, orientation):
        super().__init__((2,2,2))
        super().create_graph(False)

        self.location = location
        self.orientation = orientation

        self.create_graph()

    def create_graph(self):
        center_index = (1 - int(self.orientation[0])) + 2 * (1 - int(self.orientation[1])) + 4 * (1 - int(self.orientation[2]))

        self.node_list[center_index] = self

        for i in range(3):
            node_index = center_index - (2 ** i) + (2 ** (i + 1)) * int(self.orientation[i])

            self.node_list[node_index] = self
            self.edge_list.append((center_index, node_index))
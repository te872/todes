import numpy as np
from math import floor, cos, sin

from cubic_sphere.graph import Graph
from cubic_sphere.interface import Interface

class Element(Graph):
    def __init__(self, element, spacing, f, constraints, applied_forces, graph):
        self.id = 0
        self.weight = f
        self.graph_size = element.graph_size
        self.location = element.location
        self.node_list = element.node_list
        self.edge_list = element.edge_list
        self.spacing = 4*spacing
        self.constraints = constraints
        self.applied_forces = applied_forces

        #get node information and a dictionary of nodes belonging to this element (local indeces)
        self.active_node_list = {}
        self.node_coords, self.active_node_list = self.get_nodes_info(graph)

        #the following three variables are used during optimization and should be reset after every optimization iteration
        self.stiffness_matrices = {}
        self.other_ids = []
        self.other_weights = []
        
        #set inertial data
        self.mass_density = 2000.0
        self.calculate_center_of_mass()
        self.calculate_moment_of_inertia()
        self.mass_matrix = np.zeros((6,6))
        self.mass_matrix[0, 0] = self.mass_matrix[1, 1] = self.mass_matrix[2, 2] = self.mass
        self.mass_matrix[3:6, 3:6] = 100.0*self.I

    def get_nodes_info(self, graph):
        node_coords = {}
        active_nodes = {}

        for i in range(self.graph_size[0] * self.graph_size[1] * self.graph_size[2]):
            if  self.node_list[i] != True and self.node_list[i] != False:
                node_coords[i] = self.calculate_global_coordinates(i, graph)
                active_nodes[i] = True

        return node_coords, active_nodes

    def set_interfaces(self, graph, interface_list):
        neighbours = [0, 0, 0]
        layer_size = graph.graph_size[0] * graph.graph_size[1]
        total_size = layer_size * graph.graph_size[2]

        for i in self.active_node_list.keys():

            global_index = self.get_global_index(i, graph)

            #check whether next x-coordinate is within the same row
            if (global_index + 1) % graph.graph_size[0] > 0:
                neighbours[0] = global_index + 1
            #check whether next y-coordinate is within the same layer
            if (global_index + graph.graph_size[0]) % layer_size > global_index % layer_size:
                neighbours[1] = global_index + graph.graph_size[0]
            neighbours[2] = global_index + layer_size

            for j in neighbours:
                if j < total_size and j > 0:
                    #check whether own index and the neighbouring index point to the same element
                    if graph.node_list[global_index] == graph.node_list[j]:
                        continue

                    #check whether neighbouring index points to an element
                    if type(graph.node_list[j]).__bases__[0] == Graph:
                        interface_list.append(Interface((global_index, j), self.spacing/4, graph))

        return interface_list

    def get_coords_from_global_index(self, index, graph):
        return self.node_coords[self.get_local_index(index, graph)]

    def get_global_index(self, index, graph):
        loc = self.location
        global_coords = [-1, -1, -1]

        global_coords[0] = (index % self.graph_size[0])
        global_coords[1] = floor(index / self.graph_size[0]) % self.graph_size[1] * graph.graph_size[0]
        global_coords[2] = floor(index / (self.graph_size[0] * self.graph_size[1])) * (graph.graph_size[0] * graph.graph_size[1])

        return loc + sum(global_coords)

    def get_local_index(self, global_index, graph):
        dz = floor((global_index - self.location)/(graph.graph_size[0] * graph.graph_size[1]))
        dy = floor(((global_index - self.location)%(graph.graph_size[0] * graph.graph_size[1]))/graph.graph_size[0])
        dx = ((global_index - self.location)%(graph.graph_size[0] * graph.graph_size[1]))%graph.graph_size[0]

        return dx + dy * self.graph_size[0] + dz * self.graph_size[0] * self.graph_size[1]


    def calculate_global_coordinates(self, index, graph):
        global_index = self.get_global_index(index, graph)
        x = global_index % graph.graph_size[0] * self.spacing/4
        y = floor(global_index / graph.graph_size[0]) % graph.graph_size[1] * self.spacing/4
        z = floor(global_index / (graph.graph_size[0] * graph.graph_size[1])) % graph.graph_size[2] * self.spacing/4

        return np.array((float(x),float(y),float(z))) + self.spacing/8

    def calculate_center_of_mass(self):
        center_of_mass = [0.0,0.0,0.0]

        for node_coords in self.node_coords.values():
            center_of_mass[0] += node_coords[0]
            center_of_mass[1] += node_coords[1]
            center_of_mass[2] += node_coords[2]

        for i in range(3):
            center_of_mass[i] = center_of_mass[i] / len(self.active_node_list)

        self.center_of_mass = np.array(center_of_mass)
        self.mass = 0.5 * (self.spacing ** 3.0) * self.mass_density

    def calculate_moment_of_inertia(self):
        I = np.zeros((3, 3))

        #value is calculated for a unit CS and scaled by the element spacing and mass density
        I[0,0] = I[1,1] = I[2,2] = (self.spacing ** 5.0) * self.mass_density * 0.05208333

        self.I = I

    def translate_element(self, u):
        self.center_of_mass += u
        for node in self.node_coords.keys():
            self.node_coords[node] += u

    def rotate_element(self, phi):
        g = self.center_of_mass
        
        Rx = np.zeros((3,3))
        Ry = np.zeros((3,3))
        Rz = np.zeros((3,3))

        Rx[0,0] = 1.0
        Rx[1,1] = Rx[2,2] = cos(phi[0])
        Rx[1,2] = -sin(phi[0])
        Rx[2,1] = sin(phi[0])

        Ry[0,0] = Ry[2,2] = cos(phi[1])
        Ry[1,1] = 1.0
        Ry[0,2] = sin(phi[1])
        Ry[2,0] = -sin(phi[1])

        Rz[0,0] = Rz[1,1] = cos(phi[2])
        Rz[2,2] = 1.0
        Rz[0,1] = -sin(phi[2])
        Rz[1,0] = sin(phi[2])

        rotations = (Rx, Ry, Rz)

        for node in self.node_coords.keys():
            for R in rotations:
                s = self.node_coords[node]
                d = s - g
                self.node_coords[node] = np.matmul(R, d) + g

    def save_stiffness_matrix(self, matrix, other_id, other_weight):
        if other_id in self.stiffness_matrices:
            self.stiffness_matrices[other_id] += matrix
        else:
            self.stiffness_matrices[other_id] = matrix
            self.other_ids.append(other_id)
            self.other_weights.append(other_weight)
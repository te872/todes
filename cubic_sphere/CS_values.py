import numpy as np

cubes = []
scale = 1.0

for i in range(8):
    for j in range(4):

        bin = "{:03b}".format(i)
        a = 2*int(bin[0]) - 1
        b = 2*int(bin[1]) - 1
        c = 2*int(bin[2]) - 1
        abc = (a,b,c)

        P = scale*np.array([a*0.125,b*0.125,c*0.125])

        if j > 0:
            P[j - 1] += scale*0.25*abc[j - 1]

        cubes.append(P)

V_total = len(cubes) * ((0.25 * scale) ** 3.0)
I = np.zeros((3,3))

for cube in cubes:
    I[0,0] += (scale/4) ** 3 * (1/6 * (scale/4) ** 2 + cube[1]**2 + cube[2]**2)
    I[1,1] += (scale/4) ** 3 * (1/6 * (scale/4) ** 2 + cube[0]**2 + cube[2]**2)
    I[2,2] += (scale/4) ** 3 * (1/6 * (scale/4) ** 2 + cube[0]**2 + cube[1]**2)
    I[0,1] -= (scale/4) ** 3 * cube[0] * cube[1]
    I[0,2] -= (scale/4) ** 3 * cube[0] * cube[2]
    I[1,2] -= (scale/4) ** 3 * cube[1] * cube[2]
    I[1,0] = I[0,1]
    I[2,0] = I[0,2]
    I[2,1] = I[1,2]

print(round(V_total,10))
print(np.around(I,10))
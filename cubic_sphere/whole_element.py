from math import floor
from cubic_sphere.graph import Graph
from cubic_sphere.half_element import Half_element

class Whole_element(Graph):
    def __init__(self, location):
        self.graph_size = (4, 4, 4)
        self.location = location
        
        super().create_graph(False)
        self.create_graph()

    def create_graph(self):
        for i in range(2):
            half_normal = 0
            half_orientation = bool(i)
            half_location = 2 * i

            half = Half_element(half_location, half_normal, half_orientation)
            super().apply_subgraph_to_graph(half)

        for i in range(16):
            v1 = 1 + 4 * i
            v2 = v1 + 1

            if type(self.node_list[v1]).__bases__[0] == Graph and type(self.node_list[v2]).__bases__[0] == Graph:
                self.edge_list.append((v1, v2))
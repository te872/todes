import plotly.graph_objects as go
from math import floor

from graph import Graph
from molecule import Molecule
from quarter_element import Quarter_element
from half_element import Half_element
from whole_element import Whole_element

graph = Graph((2,2,2))
graph.create_graph(False)
molecule = Molecule(0, (False, False, False))
quarter = Quarter_element(0, 0, (False, False))
# half = Half_element(0, 2, False)
# graph.apply_subgraph_to_graph(molecule)
graph.apply_subgraph_to_graph(molecule)

x_nodes = [i % graph.graph_size[0] * 20 for i in range(len(graph.node_list))]
y_nodes = [floor(i / graph.graph_size[0]) % graph.graph_size[1] * 20 for i in range(len(graph.node_list))]
z_nodes = [floor(i / (graph.graph_size[0] * graph.graph_size[1])) % graph.graph_size[2] * 20 for i in range(len(graph.node_list))]

#we  need to create lists that contain the starting and ending coordinates of each edge.
x_edges=[]
y_edges=[]
z_edges=[]

#need to fill these with all of the coordiates
for edge in graph.edge_list:
    #format: [beginning,ending,None]
    x_coords = [x_nodes[edge[0]], x_nodes[edge[1]],None]
    x_edges += x_coords

    y_coords = [y_nodes[edge[0]], y_nodes[edge[1]],None]
    y_edges += y_coords

    z_coords = [z_nodes[edge[0]], z_nodes[edge[1]],None]
    z_edges += z_coords

nodes_int = [int(graph.node_list[i]) for i in range(len(graph.node_list))]

#create a trace for the edges
trace_edges = go.Scatter3d(x=x_edges,
                        y=y_edges,
                        z=z_edges,
                        mode='lines',
                        line=dict(color='black', width=2),
                        hoverinfo='none')

trace_nodes = go.Scatter3d(x=x_nodes,
                         y=y_nodes,
                        z=z_nodes,
                        mode='markers',
                        marker=dict(symbol='circle',
                                    size=5,
                                    color= nodes_int, #color the nodes according to their community
                                    colorscale=['red','green'], #either green or mageneta
                                    line=dict(color='black', width=0.5)),
                        text="test",
                        hoverinfo='text')

#we need to set the axis for the plot 

layout = go.Layout(title="Graph representing the masonry elements",
                width=1500,
                height=700,
                showlegend=False,
                scene=dict(xaxis=dict(  backgroundcolor="white",
                                        gridcolor="rgb(200,200,200)",
                                        title='x'),
                        yaxis=dict(     backgroundcolor="white",
                                        gridcolor="rgb(200,200,200)",
                                        title='y'),
                        zaxis=dict(     backgroundcolor="white",
                                        gridcolor="rgb(200,200,200)",
                                        title='z'),
                        ),
                margin=dict(t=100),
                hovermode='closest')

data = [trace_edges, trace_nodes]
fig = go.Figure(data=data, layout=layout)

fig.show()